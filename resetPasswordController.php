<?php
include "config.php";
if ($_POST['senha'] === $_POST['senhaconf']){
    $query = "SELECT id FROM user WHERE forgot_pass_identity = '" . $_POST['code'] . "'";
    $resultado = mysqli_query($link,$query); // Executa a query $query na conexão $db
    $linha = mysqli_fetch_assoc($resultado); 
    if (isset($linha) && isset($linha['id'])){
        $hash = crypt($_POST['senha'], '$2a$08$Cf1f11ePArKlBJomM0F6aJ$');
        $query = "UPDATE user SET password = '" . $hash . "', forgot_pass_identity = '' WHERE id = '". $linha['id'] ."'";
        $resultado = mysqli_query($link,$query); // Executa a query $query na conexão $db
        if($resultado) {
            echo '
            <script>
                window.alert("Senha alterada com sucesso!");
                window.location = "login.php";
            </script>
            ';
        }else{
            echo '
            <script>
                window.alert("Erro ao alterar senha");
                window.location = "login.php";
            </script>
            ';
        }
    }else {
        echo '
        <script>
            window.alert("O link não é mais válido!");
            window.location = "password.php";
        </script>
        ';
    }
}else{
    echo '
    <script>
    window.alert("As senhas não conferem!");
    window.location = "javascript:history.go(-1)";
</script>
';
}