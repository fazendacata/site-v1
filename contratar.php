<?php
include "header.php";
include "security.php";
$idplan = $_GET['idplan'];
if (isset($idplan)){
    $query = "SELECT id, descricao, valor, link_boleto, link_cartao, quantidade FROM produto WHERE id = " . $idplan;
    $resultado = mysqli_query($link, $query);
    $plano = mysqli_fetch_assoc($resultado);
    $query = "SELECT id, descricao FROM pagamento";
    $resultado = mysqli_query($link, $query);
    $pagamentos = [];
    while($linha = mysqli_fetch_array($resultado)){
        array_push($pagamentos, $linha);
    }
}
?>

<div class="content slide">
    <ul class="responsive">
        <li class="users-section">
            <div class="plan-box">
                <h3><?=$plano['descricao']?></h3>
                <h2>Valor mensal: <strong>R$<?=$plano['valor']?>,00</strong></h2>
                <h2>Valor total: <strong>R$<?=($plano['quantidade']*$plano['valor'])?>,00</strong></h2>
                <div class="buttons">
                    <select name="pagamento" id="pagamento" <?=($ambiente == 'prod') ? "hidden":""?>>
                        <option value="0" selected>*Selecione uma opção</option>
                        <?php
                        for ($i=0; $i <sizeof($pagamentos) ; $i++) { 
                            echo'
                            <option value="'.$pagamentos[$i]['id'] .'">'.$pagamentos[$i]['descricao'] .'</option>
                            ';
                        }
                        ?>
                    </select>
                    <form action="<?=$plano['link_boleto']?>" method="get">
                        <input type="submit" value="Contratar" hidden id="contratar-button-boleto">
                    </form>
                    <form action="<?=$plano['link_cartao']?>" method="get">
                        <input type="submit" value="Contratar" hidden id="contratar-button-cartao">
                    </form>    
                </div>
                <h3 id="nopay" <?=($ambiente != 'prod') ? "hidden":""?>>Em breve estaremos recebendo pagamentos online.</h3>
                <h3 id="nopay" <?=($ambiente != 'prod') ? "hidden":""?>>Para adquirir nosso sistema ou utilizar uma versão de teste, entre em <a href="index.php#contato">contato</a> conosco.</h3>
            </div>
        </li>
    </ul>
</div>

<?php
include 'footer.php';
?>

<script>
    $(document).ready(function(){
        $('select[name=pagamento]').click(function () {
            // hide all optional elements
            $('.member-type').hide();
            $('.arena-member-type').hide();
            $('.bs-member-type').hide();
            $('.pro-member-type').hide();
                
            $("select[name=pagamento] option:selected").each(function () {
                var value = $(this).val();
                if(value == "0") {
                    $('#contratar-button-boleto').hide();
                    $('#contratar-button-cartao').hide();
                } else if(value == "1") {
                    $('#contratar-button-cartao').hide();
                    $('#contratar-button-boleto').show();
                } else if(value == "2") {
                    $('#contratar-button-boleto').hide();
                    $('#contratar-button-cartao').show();
                }
            });
        });
    });
</script>