<?php 
include "header.php";
include "admin_security.php";
$query = "SELECT name, email, active, permissao FROM user";
$resultado = mysqli_query($link, $query);
if (!$resultado) {
    die('Invalid query: ' . mysqli_error($link));
}
$usuarios = [];
while($linha = mysqli_fetch_array($resultado)){
    $linha['permissao'] = mysqli_fetch_array(mysqli_query($link, "SELECT descricao FROM permissao WHERE id=".$linha['permissao']))['descricao'];
    array_push($usuarios, $linha);
}

?>

<div class="content slide">
    <ul class="responsive">
        <li class="users-section">
            <table class="table-users">
                <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Permissão</th>
                    <th scope="col">Ativo</th>
                </tr>
                <?php
                for ($i=0; $i < sizeof($usuarios) ; $i++) { 
                    echo '<tr>
                        <td width="300" data-title="Nome">'.$usuarios[$i]['name'].'</td>
                        <td width="300" data-title="E-mail">'.$usuarios[$i]['email'].'</td>
                        <td data-title="Permissão">'.$usuarios[$i]['permissao'].'</td>
                        <td data-title="Ativo">'.($usuarios[$i]['active'] == 1 ? "Sim":"Não").'</td>
                    </tr>';
                }
                ?>
            </table>
        </li>
    </ul>
</div>

<?php
include 'footer.php';
?>