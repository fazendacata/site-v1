<?php
include "header.php"; 
?>
<div class="content slide">
<ul class="responsive">
    <li class="body-section body-contato">
        <form action="contatoController.php" method="post" class="contato-form">
            <input type="text" name="titulo" placeholder="Título" required>
            <br>
            <br>
            <input type="email" name="e-mail" placeholder="E-mail para contato" required value="<?=_(isset($user) ? $user['email'] : "")?>">
            <br>
            <br>
            <input type="text" name="name" placeholder="Seu nome" required value="<?=_(isset($user) ? $user['name'] : "")?>">
            <br>
            <br>
            <textarea name="mensagem" cols="50" rows="10" placeholder="Mensagem" required></textarea>
            <br>
            <br>
            <input type="submit" value="Enviar" >
        </form>
    </li>

<?php
include "footer.php";
?>