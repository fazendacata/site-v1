<?php
include "config.php";
if (isset($_POST['senha'])){
    $hash = crypt($_POST['senha'], '$2a$08$Cf1f11ePArKlBJomM0F6aJ$');
    $query = "SELECT id, name, email, username, active FROM user WHERE email = '" . $_POST['email']. "' AND password = '" . $hash. "'";
    $resultado = mysqli_query($link, $query);
    if (!$resultado) {
        die('Invalid query: ' . mysqli_error($link));
    }
    $linha = mysqli_fetch_assoc($resultado);
    mysqli_close($link);
    if (isset($linha) && isset($linha['id'])){
        if ($linha['active']){
            $user = [];
            $user['id'] = $linha['id'];
            $user['name'] = $linha['name'];
            $user['email'] = $linha['email'];
            $user['username'] = $linha['username'];
            $cookie = setcookie('user_cookie', serialize($user), time() + (60 * 30), '/');
            echo "
            <script>
                window.location = 'index.php';
            </script>
            ";
        } else {
            echo "
            <script>".
                // window.location = \"mailConfirm.php?email=".$linha['email'] . "\";
            "window.alert('Este usuário está desativado.');
            window.location = 'login.php';".
            "</script>
            ";
        }
    }else{
        echo "
            <script>
                window.alert('Usuário ou senha inválido.');
                window.location = 'login.php';
            </script>
        ";
    }
}else {
    echo '
    <script>
        window.location = "javascript:history.go(-1)"
    </script>
    ';
}