<?php
include "config.php";
if (isset($_POST['email'])){
    $query = "SELECT id, email, name FROM user WHERE email = '" . $_POST['email']. "'";
    $resultado = mysqli_query($link,$query); // Executa a query $query na conexão $db
    $user = mysqli_fetch_assoc($resultado); //aqui troquei para arrays, este loop declara a variavel $linha (ela representa o resultado da query), e o loop lê linha a linha do retorno
    // Escreve na página o retorno para cada registro trazido pela query
    if (isset($user['id'])){
        $uniqidStr = md5(uniqid(mt_rand()));
        
        $query = "UPDATE user SET forgot_pass_identity = '" . $uniqidStr. "' WHERE id='" .$user['id'] . "'";
        $resultado = mysqli_query($link,$query); // Executa a query $query na conexão $db
        if ($resultado){
            $resetPassLink = $address.'/resetPassword.php?fp_code='.$uniqidStr;
            //send reset password email
            $to = $user['email'];
            $subject = "Pedido de Redefinição de Senha";
            $mailContent = 'Olá '.$user['name'].', 
            <br/>Um pedido de redefinição de senha para seu e-mail foi feito. Se isto for um engano, ignore esse e-mail.
            <br/>Para redefinir sua senha, clique no link abaixo:<br/> <a href="'.$resetPassLink.'">'.$resetPassLink.'</a>
            <br/><br/>Atenciosamente,
            <br/>Café - PBC';
            $mail->Subject  = utf8_decode($subject);
            $mail->Body  = utf8_decode($mailContent);
            $mail->AddAddress($to,utf8_decode($user['name']));
            if(!$mail->Send()){
                $mensagemRetorno = 'Erro ao enviar formulário: '. print($mail->ErrorInfo);
            }else{
                $mensagemRetorno = 'Um e-mail foi enviado para ' . $to . '. <br>Por favor verifique sua caixa de entrada e spam.';
            } 
            $sessData['status']['type'] = 'success';
            $sessData['status']['msg'] = $mensagemRetorno;
            $sessData['email'] = $to;
            $_SESSION['sessData'] = $sessData;
        }
    }else {
        echo "
            <script>
                window.alert('E-mail não cadastrado.');
                window.location = 'forgot.php';
            </script>
        ";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/login-reg.css" rel="stylesheet">
    </head>
    <body>
    <div class="lgn-container">
        <div class="lgn-content">
            <h2>E-mail enviado!</h2>
            <h4><?= $mensagemRetorno ?></h4>
        </div>
    </div>
    
    </body>
</html>