<?php
include "config.php";
if (isset($_GET['email'])){
    $email = $_GET['email'];
    $query = "SELECT name, active FROM user WHERE email = '" . $email. "'";
    $resultado = mysqli_query($link,$query); // Executa a query $query na conexão $db
    $user = mysqli_fetch_assoc($resultado); 
    if ($user['active'] == '0'){
        $uniqidStr = md5(uniqid(mt_rand()));
        $mailConfirmLink = $address.'/confirmMail.php?mc_code='.$uniqidStr;
        
        $query = "UPDATE user SET mail_confirm_identity = '" . $uniqidStr. "' WHERE email='" .$email . "'";
        $resultado = mysqli_query($link,$query); // Executa a query $query na conexão $db
        //send reset password email
        $to = $email;
        $subject = "Confirmação de e-mail";
        $mailContent = 'Olá '.$user['name'].', 
        <br/>Um cadastro em nosso sistema foi feito com esse endereço de e-mail. Se você não realizou esse cadastro, ignore esta mensagem.
        <br/>Para utilizar nosso sistema pedimos que você confirme seu e-mail.
        <br/>Para confirmar seu endereço de e-mail, clique no link abaixo:<br/> <a href="'.$mailConfirmLink.'">'.$mailConfirmLink.'</a>
        <br/><br/>Atenciosamente,
        <br/>Café - PBC';
       
        $mail->Subject  = utf8_decode($subject);
        $mail->Body  = utf8_decode($mailContent);
        $mail->AddAddress($to,utf8_decode($user['name']));
        if(!$mail->Send()){
            $mensagemRetorno = 'Erro ao enviar formulário: '. print($mail->ErrorInfo);
        }else{
            $mensagemRetorno = 'Um e-mail foi enviado para ' . $to . '. <br>Por favor verifique sua caixa de entrada e spam.';
        } 
        $sessData['status']['type'] = 'success';
        $sessData['status']['msg'] = $mensagemRetorno;
        $sessData['email'] = $to;
        $_SESSION['sessData'] = $sessData;
    } else {
        echo "
            <script>
                window.location = 'login.php';
            </script>
        ";
    }
}else {
    echo "
        <script>
            window.location = 'login.php';
        </script>
    ";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/login-reg.css">
    <title>Confirmação de e-mail</title>
    <link rel="shortcut icon" type="image/png" href="images/Coffee.png"/>
</head>
<body>
    <div class="lgn-container">
        <div class="lgn-content">
            <h2>Confirmação de e-mail</h2>
            <h4>Você ainda não confirmou seu e-mail.</h4>
            <h4><?= $mensagemRetorno?></h4>
            <div class="container">
                <div class="regisFrm">
                </div>
            </div>
        </div>
    </div>
</body>
</html>