<?php
include "config.php";
if (isset($_POST['e-mail'])){
    $mail->FromName  = $_POST['name'];
    $to = "contato@cafepbc.com.br";
    $subject = $_POST['titulo'];
    $mailContent = "Nome: ".$_POST['name']."<br>" ."E-mail para contato: ". $_POST['e-mail'] . "<br><br>" . $_POST['mensagem'];
   
    $mail->Subject  = utf8_decode($subject);
    $mail->Body  = utf8_decode($mailContent);
    $mail->AddAddress($to,utf8_decode('Contato Café PBC'));
    if(!$mail->Send()){
        $mensagemRetorno = 'Erro ao enviar formulário: '. print($mail->ErrorInfo);
    }else{
        $mensagemRetorno = 'Obrigado por entrar em contato.<br>Em breve um dos membros de nossa equipe irá responder.<br>Aguarde, você será redirecionado.';
        header( "refresh:3;url=index.php" );
    } 
    $sessData['status']['type'] = 'success';
    $sessData['status']['msg'] = $mensagemRetorno;
    $sessData['email'] = $to;
    $_SESSION['sessData'] = $sessData;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/login-reg.css">
    <title>Confirmação de e-mail</title>
</head>
<body>
    <div class="lgn-container">
        <div class="lgn-content">
            <h2>Mensagem enviada</h2>
            <h4><?= $mensagemRetorno?></h4>
            <div class="container">
                <div class="regisFrm">
                </div>
            </div>
        </div>
    </div>
</body>
</html>