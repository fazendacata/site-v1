<?php
include "header-login.php";
?>
<body>
    <div class="container">
        <div class="row">
            <div class="login-box">
                <a href="index.php">
                    <div class="logo">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <img src="images/Coffee.png" alt="" class="img-responsive">
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">
                            <h3>Café - PBC</h3>
                        </div>
                    </div>
                </a>
                <div class="clearfix"></div>
                <form action="passwordController.php" method="post" class="login-form">
                    <input type="email" name="email" placeholder="Email" required>
                    <br>
                    <br>
                    <input type="submit" value="Enviar" >
                </form>
            </div>
        </div>
    </div>
</body>
</html>