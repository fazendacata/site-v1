<?php
include "header-login.php";
?>
<body>
    <div class="container">
        <div class="row">
            <div class="login-box">
                <a href="index.php">
                    <div class="logo">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <img src="images/Coffee.png" alt="" class="img-responsive">
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">
                            <h3>Café - PBC</h3>
                        </div>
                    </div>
                </a>
                <div class="clearfix"></div>
                <form action="resetPasswordController.php" method="post" class="login-form">
                    <input type="password" name="senha" placeholder="Nova senha" required pattern=".{6,}"  title="Mínimo de 6 dígitos">
                    <br>
                    <br>
                    <input type="password" name="senhaconf" placeholder="Confirmação da nova senha" required pattern=".{6,}"  title="Mínimo de 6 dígitos">
                    <br>
                    <br>
                    <input type="text" name="code" hidden value="<?=$_GET['fp_code']?>">
                    <br>
                    <br>
                    <input type="submit" value="Enviar" >
                </form>
                <h5 class="center">Já é usuário? <a href="login.php">Faça login</a></h5>
            </div>
        </div>
    </div>
</body>
</html>