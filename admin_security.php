<?php 
if (isset($_COOKIE['user_cookie'])){
    $permission = $user['permissao'];
    if ($permission != 1){
        echo "
            <script>
            window.alert('É necessário ser um administrador para acessar essa página.');
            window.location = 'index.php'
            </script>
        ";
    }
} else {
    echo "
        <script>
        window.location = 'login.php'
        </script>
    ";
}