<?php
include "header.php";
?>

		<div class="content slide">
			<ul class="responsive">
				<li class="header-section">
					<br>
					<h1 class="title">Sistemas de Produção, Beneficiamento e Comercialização de Café</h1>
					<p class="placefiller">
						<section class="main circles">
							<ul class="ch-grid">
								<li>
									<div class="ch-item ch-img-1">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-1"></div>
												<div class="ch-info-back">
													<h3>Android</h3>
													<p>
														Controle sua produção e gerencie suas vendas pelo celular.
														<!-- <a href="http://drbl.in/eFDk">View on Dribbble</a> -->
													</p>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="ch-item ch-img-2">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-2"></div>
												<div class="ch-info-back">
													<h3>Desktop</h3>
													<p>
														Controle sua produção e gerencie suas vendas com um sistema completo.
														<!-- <a href="http://drbl.in/eFDk">View on Dribbble</a> -->
													</p>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="ch-item ch-img-3">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-3"></div>
												<div class="ch-info-back">
													<h3>Web</h3>
													<p>
														Controle sua produção e gerencie suas vendas de qualquer lugar.
														<!-- <a href="http://drbl.in/eFDk">View on Dribbble</a> -->
													</p>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>

						</section>
						<br>
						<br>
						<br>
						<br>
					</p>
					<!-- <img src="images/arrow.gif" alt="" class="arrow"> -->
				</li>
				<li class="body-section" id="empresa">
					<h1 class="title">Empresa</h1>
					<br>
					<br>
					<ul class="empresa">
						<li>
							- Fundada em Alfenas-MG, com a iniciativa de auxiliar produtores de café da região do sul de minas.
						</li>
						<br>
						<li>
							- Nosso objetivo é ajudar agricultores, produtores e distribuidores de café a gerenciar sua produção, desde o cultivo até
							a distribuição e controle de gastos.
						</li>
						<br>
						<li>
							- Somos apaixonados por agricultura, e sabemos o quanto é importante um sistema que permita você administrar seus ganhos
							de qualquer lugar e SEM INTERNET.
						</li>
						<br>
						<li>
							- Com nossos sistemas para Android e Windows, você pode controlar tudo, tudo mesmo, e sem conexão com a internet.
						</li>
						<br>
						<li>
							- Usamos sua conexão de dados apenas para sincronizar sua conta, realizando backups de seus dados para que você possa acessar a qualquer momento e de qualquer lugar.
						</li>
					</ul>

					<section class="main">
							<ul class="ch-grid">
								<li>
									<div class="ch-item ch-img-4">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-4"></div>
												<div class="ch-info-back">
													<h3>Arthur Busqueiro</h3>
													<p>
													Fundador
													<br>
													Desenvolvedor
														<a target="_blank" href="https://www.linkedin.com/in/arthurbusqueiro/">Linkedin</a>
													</p>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li>
									<!-- <div class="ch-item ch-img-2">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-2"></div>
												<div class="ch-info-back">
													<h3>Desktop</h3>
													<p>
														Controle sua produção e gerencie suas vendas com um sistema completo.
													</p>
												</div>
											</div>
										</div>
									</div> -->
								</li>
								<li>
									<div class="ch-item ch-img-5">
										<div class="ch-info-wrap">
											<div class="ch-info">
												<div class="ch-info-front ch-img-5"></div>
												<div class="ch-info-back">
													<h3>Rodolfo Bueno de Oliveira</h3>
													<p>
														Fundador
														<br>
														Desenvolvedor
														<a target="_blank" href="https://www.linkedin.com/in/rodolfo-bueno-de-oliveira-8579b85b/">Linkedin</a>
													</p>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>

						</section>
				</li>

				<li class="body-section body-contato" id="contato">
					<h1 class="title contato">Fale conosco</h1>
					<br>
					<br>
					<form action="contatoController.php" method="post" class="contato-form">
						<input type="text" name="titulo" placeholder="Título" required>
						<br>
						<br>
						<input type="email" name="e-mail" placeholder="E-mail para contato" required value="<?=_(isset($user) ? $user['email'] : "")?>">
						<br>
						<br>
						<input type="text" name="name" placeholder="Seu nome" required value="<?=_(isset($user) ? $user['name'] : "")?>">
						<br>
						<br>
						<textarea name="mensagem" cols="50" rows="10" placeholder="Mensagem" required></textarea>
						<br>
						<br>
						<input type="submit" value="Enviar" >
					</form>
				</li>
	<?php
	include "footer.php";
	?>