<?php 
include 'config.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<title>Café - Produção, Beneficiamento e Comercialização</title>

	<meta charset="utf-8">
	<meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1" media="(device-height: 568px)">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="shortcut icon" type="image/png" href="images/Coffee.png"/>

	<!-- Style Sheets -->
	<link rel="stylesheet" type="text/css" media="all" href="css/reset.css" />
	<link rel="stylesheet" type="text/css" media="all" href="css/trunk.css" />
	<!-- <link rel="stylesheet" type="text/css" href="css/demo.css" /> -->
	<link rel="stylesheet" type="text/css" href="css/common.css" />
	<link rel="stylesheet" type="text/css" href="css/style4.css" />

	<!-- Scripts -->
	<script type="text/javascript">
		if (typeof jQuery == 'undefined')
			document.write(unescape("%3Cscript src='js/jquery-1.9.js'" +
				"type='text/javascript'%3E%3C/script%3E"))
	</script>
	<script type="text/javascript" language="javascript" src="js/trunk.js"></script>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116727988-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	
	  gtag('config', 'UA-116727988-1');
	</script>
	<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->

</head>

<body>

	<div class="my-container">

    <header class="slide">
        <ul id="navToggle" class="burger slide">
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <a href="index.php">
            <img src="images/Coffee.png" alt="" class="logo">
        </a>
        <h1 class="name">Café - PBC</h1>
    </header>

    <nav class="slide">
        <ul>
            <li>
                <a href="index.php" class="active">Início</a>
            </li>
            <li>
                <a href="index.php#empresa">Empresa</a>
            </li>
            <li>
                <a href="#contato">Contato</a>
            </li>
            <li>
                <a href="planospbc.html">Planos</a>
            </li>
            <?php
            if (!isset($user)){
                ?>
                <li>
                    <a href="login.php">Entrar</a>
                </li>
            <?php
            }else{
                ?>
                <li class="relative dropdown">
                    <img src="images/default-profile.png" alt="" class="img-responsive profile">
                    <span class="perfil"><?=_(explode(" ", $user['name'])[0])?></span>	
                    <div class="dropdown-content">
                        <a class="content-dropdown" href="downloads.php">Downloads</a>
                        <?php 
                        if($user['permissao'] == 1){
                            echo '<a class="content-dropdown" href="usuarios.php">Usuarios</a>';

                        }
                        ?>
                        <a class="content-dropdown" href="logout.php">Sair</a>
                    </div>
                </li>
            <?php
            }
            ?>
        </ul>
    </nav>
	