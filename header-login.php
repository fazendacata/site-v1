<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Café - Produção, Beneficiamento e Comercialização</title>
    <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" media="all" href="css/login.css" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116727988-1"></script>
    <link rel="shortcut icon" type="image/png" href="images/Coffee.png"/>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	
	  gtag('config', 'UA-116727988-1');
	</script>
</head>